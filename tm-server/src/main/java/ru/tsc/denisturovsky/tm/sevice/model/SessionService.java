package ru.tsc.denisturovsky.tm.sevice.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.denisturovsky.tm.api.repository.model.ISessionRepository;
import ru.tsc.denisturovsky.tm.api.service.IConnectionService;
import ru.tsc.denisturovsky.tm.api.service.model.ISessionService;
import ru.tsc.denisturovsky.tm.model.Session;
import ru.tsc.denisturovsky.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

}